namespace AndroidMarket.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Applications",
                c => new
                    {
                        ApplicationId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        ApkName = c.String(nullable: false, maxLength: 100),
                        Size = c.Long(nullable: false),
                        Thumbnail = c.String(maxLength: 100),
                        PackageName = c.String(maxLength: 200),
                        Description = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.ApplicationId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Applications");
        }
    }
}
