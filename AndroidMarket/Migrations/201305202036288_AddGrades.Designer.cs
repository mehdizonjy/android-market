// <auto-generated />
namespace AndroidMarket.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class AddGrades : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddGrades));
        
        string IMigrationMetadata.Id
        {
            get { return "201305202036288_AddGrades"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
