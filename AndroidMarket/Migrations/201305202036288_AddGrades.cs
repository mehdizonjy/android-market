namespace AndroidMarket.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGrades : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Grades",
                c => new
                    {
                        GradeId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.GradeId);
            
            AddColumn("dbo.Applications", "Grade_GradeId", c => c.Int(nullable: false));
            AddForeignKey("dbo.Applications", "Grade_GradeId", "dbo.Grades", "GradeId", cascadeDelete: true);
            CreateIndex("dbo.Applications", "Grade_GradeId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Applications", new[] { "Grade_GradeId" });
            DropForeignKey("dbo.Applications", "Grade_GradeId", "dbo.Grades");
            DropColumn("dbo.Applications", "Grade_GradeId");
            DropTable("dbo.Grades");
        }
    }
}
