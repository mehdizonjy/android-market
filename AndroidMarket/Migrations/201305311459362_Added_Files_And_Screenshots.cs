namespace AndroidMarket.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Files_And_Screenshots : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Screenshots",
                c => new
                    {
                        ScreenshotId = c.Int(nullable: false, identity: true),
                        Application_ApplicationId = c.Int(nullable: false),
                        File_ImageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ScreenshotId)
                .ForeignKey("dbo.Files", t => t.File_ImageId, cascadeDelete: true)
                .ForeignKey("dbo.Applications", t => t.Application_ApplicationId, cascadeDelete: true)
                .Index(t => t.File_ImageId)
                .Index(t => t.Application_ApplicationId);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        FileId = c.Int(nullable: false, identity: true),
                        StorageName = c.String(nullable: false, maxLength: 256),
                        FileName = c.String(nullable: false, maxLength: 256),
                        Size = c.Long(nullable: false),
                        DownloadedCounter = c.Long(nullable: false),
                        FileType = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.FileId);
            
            AddColumn("dbo.Applications", "File_ApkId", c => c.Int(nullable: false));
            AddColumn("dbo.Applications", "File_ThumbnailId", c => c.Int(nullable: false));
            AlterColumn("dbo.Applications", "PackageName", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Applications", "Description", c => c.String(nullable: false, maxLength: 300));
            AddForeignKey("dbo.Applications", "File_ApkId", "dbo.Files", "FileId", cascadeDelete: false);
            AddForeignKey("dbo.Applications", "File_ThumbnailId", "dbo.Files", "FileId", cascadeDelete: false);
            CreateIndex("dbo.Applications", "File_ApkId");
            CreateIndex("dbo.Applications", "File_ThumbnailId");
            DropColumn("dbo.Applications", "ApkName");
            DropColumn("dbo.Applications", "Size");
            DropColumn("dbo.Applications", "Thumbnail");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Applications", "Thumbnail", c => c.String(maxLength: 100));
            AddColumn("dbo.Applications", "Size", c => c.Long(nullable: false));
            AddColumn("dbo.Applications", "ApkName", c => c.String(nullable: false, maxLength: 100));
            DropIndex("dbo.Screenshots", new[] { "Application_ApplicationId" });
            DropIndex("dbo.Screenshots", new[] { "File_ImageId" });
            DropIndex("dbo.Applications", new[] { "File_ThumbnailId" });
            DropIndex("dbo.Applications", new[] { "File_ApkId" });
            DropForeignKey("dbo.Screenshots", "Application_ApplicationId", "dbo.Applications");
            DropForeignKey("dbo.Screenshots", "File_ImageId", "dbo.Files");
            DropForeignKey("dbo.Applications", "File_ThumbnailId", "dbo.Files");
            DropForeignKey("dbo.Applications", "File_ApkId", "dbo.Files");
            AlterColumn("dbo.Applications", "Description", c => c.String(maxLength: 300));
            AlterColumn("dbo.Applications", "PackageName", c => c.String(maxLength: 200));
            DropColumn("dbo.Applications", "File_ThumbnailId");
            DropColumn("dbo.Applications", "File_ApkId");
            DropTable("dbo.Files");
            DropTable("dbo.Screenshots");
        }
    }
}
