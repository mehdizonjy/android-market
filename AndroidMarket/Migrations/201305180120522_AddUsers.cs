namespace AndroidMarket.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUsers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.UserApplications",
                c => new
                    {
                        User_UserId = c.String(nullable: false, maxLength: 128),
                        Application_ApplicationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_UserId, t.Application_ApplicationId })
                .ForeignKey("dbo.Users", t => t.User_UserId, cascadeDelete: true)
                .ForeignKey("dbo.Applications", t => t.Application_ApplicationId, cascadeDelete: true)
                .Index(t => t.User_UserId)
                .Index(t => t.Application_ApplicationId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserApplications", new[] { "Application_ApplicationId" });
            DropIndex("dbo.UserApplications", new[] { "User_UserId" });
            DropForeignKey("dbo.UserApplications", "Application_ApplicationId", "dbo.Applications");
            DropForeignKey("dbo.UserApplications", "User_UserId", "dbo.Users");
            DropTable("dbo.UserApplications");
            DropTable("dbo.Users");
        }
    }
}
