﻿using AndroidMarket.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace AndroidMarket.Models
{
    public class DbMarketContext:DbContext
    {
        public DbSet<Application> Applications { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<Screenshot> Screenshots { get; set; }
        public DbSet<File> Files { get; set; }
        public DbMarketContext()
            : base("AndroidMarketContext")
        { }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //            modelBuilder.Entity<Application>().HasRequired(a=>a.File_ThumbnailId
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

        }
       

    }
}