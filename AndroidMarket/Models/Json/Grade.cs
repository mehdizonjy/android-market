﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AndroidMarket.Models.Json
{
    public class Grade
    {
        public int GradeId { get; set; }
        public string Name { get; set; }
        public int ApplicationsCount { get; set; }

    }
}