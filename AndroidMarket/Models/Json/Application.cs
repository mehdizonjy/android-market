﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AndroidMarket.Models.Json
{
    public class Application
    {
        public int ApplicationId { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
        public string Thumbnail { get; set; }
        public string PackageName { get; set; }
        public string Description { get; set; }
        public string GradeName { get; set; }
        public List<String> Screenshots { get; set; }


        public Application()
        {
            Screenshots = new List<string>();
        }
        public Application(Entities.Application app)
            : this()
        {
            ApplicationId = app.ApplicationId;
            Name = app.Name;
            Size = app.Apk.Size;
            PackageName = app.PackageName;
            Description = app.Description;
            Thumbnail = "/content/apks/images/" + app.Thumbnail.StorageName;
            GradeName = app.Grade.Name;

            foreach (var shot in app.Screenshots)
                Screenshots.Add("/content/apks/images/" + shot.Image.StorageName);
        }

    }
}