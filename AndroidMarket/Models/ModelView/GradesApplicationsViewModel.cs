﻿using AndroidMarket.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AndroidMarket.Models.ModelViews
{
    public class GradesApplicationsViewModel
    {
         public IEnumerable<Application> Applications { get; set; }
        public IEnumerable<Grade> Grades { get; set; }

        public GradesApplicationsViewModel(IEnumerable<Application> apps, IEnumerable<Grade> grades)
        {
            this.Applications = apps;
            this.Grades = grades;
        }
    }
}