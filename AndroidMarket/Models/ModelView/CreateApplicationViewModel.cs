﻿using AndroidMarket.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AndroidMarket.Models.ModelViews
{
    public class CreateApplicationViewModel
    {
        public Application Application { get; set; }
        public IEnumerable<Grade> Grades { get; set; }
        [Required]
        public String Apk { get; set; }
        [Required]
        public String Thumbnail { get; set; }

        

        public CreateApplicationViewModel(Application app, IEnumerable<Grade> grades)
        {
            this.Application = app;
            this.Grades = grades;
        }
    }
}