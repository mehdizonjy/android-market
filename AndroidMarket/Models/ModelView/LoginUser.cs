﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AndroidMarket.Models.ModelViews
{
    public class LoginUser
    {
        [Required]
        [StringLength(10)]
        [DisplayName("اسم المستخدم")]
        public String UserName { get; set; }
        [Required]
        [StringLength(10)]
        [DisplayName("كلمة المرور")]
        public String Password { get; set; }
    }
}