﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AndroidMarket.Models.Entities
{
    public class User
    {
        [Key]
        public String UserId{get;set;}


        public virtual List<Application> Applications { get; set; }

        public User()
        {
            this.Applications = new List<Application>();
        }
    }
}