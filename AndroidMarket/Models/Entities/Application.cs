﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.ComponentModel;
using AndroidMarket.Models.Entities;

namespace AndroidMarket.Models.Entities
{
    public class Application
    {
        [Key]
        [Required]
        public int ApplicationId { get; set; }

        [StringLength(100)]
        [Required]
        [DisplayName("الاسم")]
        public string Name { get; set; }

                  
        [StringLength(200)]   
        [Required]
        public string PackageName { get; set; }

        [StringLength(300)]
        [DisplayName("الوصف")]
        [Required]
        public string Description { get; set; }

        [ForeignKey("Apk")]
        public int File_ApkId { get; set; }



        [ForeignKey("Thumbnail")]
        public int File_ThumbnailId { get; set; }


        [Required]
        [ForeignKey("Grade")]
        public int Grade_GradeId { get; set; }


        public virtual List<Screenshot> Screenshots { get; set; }
        public virtual List<User> Users { get; set; }
        public virtual Grade Grade { get; set; }
        public virtual File Apk { get; set; }
        public virtual File Thumbnail { get; set; }

             
        public Application()
        {
            this.Users = new List<User>();
        }
    }
}