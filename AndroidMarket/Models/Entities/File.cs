﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AndroidMarket.Models.Entities
{
    public class File
    {
        [Required]
        [Key]
        public int FileId { get; set; }

        [Required]
        [StringLength(256)]
        public string StorageName { get; set; }

        [Required]
        [StringLength(256)]
        public string FileName { get; set; }

        [Required]
        public long Size { get; set; }

        [Required]
        public long DownloadedCounter { get; set; }

        [Required]
        public String FileType { get; set; }
    }
}