﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AndroidMarket.Models.Entities
{
    public class Screenshot
    {
        [Key]
        [Required]
        public int ScreenshotId{get;set;}

        [Required]
        [ForeignKey("Application")]
        public int Application_ApplicationId { get; set; }

        [Required]
        [ForeignKey("Image")]
        public int File_ImageId { get; set; }

        public virtual File Image { get; set; }
        public virtual Application Application { get; set; }

    }
}