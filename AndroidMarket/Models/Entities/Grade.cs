﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AndroidMarket.Models.Entities
{
    public class Grade
    {
        [Key]
        public int GradeId{get;set;}
        [Required]

        [StringLength(50)]
        [DisplayName("الصف")]
        public String Name { get; set; }

        [JsonIgnore]
        public List<Application> Applications { get; set; }

        public Grade()
        {
            this.Applications = new List<Application>();
        }
    }
}