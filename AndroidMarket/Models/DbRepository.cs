﻿using AndroidMarket.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AndroidMarket.Models
{
    public class DbRepository :IDisposable
    {
        private DbMarketContext _context;

        public IQueryable<Application> Applications { get { return _context.Applications; } }
        public IQueryable<File> Files { get { return _context.Files; } }
        public IQueryable<User> Users { get { return _context.Users; } }
        public IQueryable<Grade> Grades { get { return _context.Grades; } }
        public IQueryable<Screenshot> Screenshots { get { return _context.Screenshots; } }

        public DbRepository()
        {
            _context = new DbMarketContext();
        }


        public Application AddApplication(Application app)
        {
            _context.Applications.Add(app);
            return app;
        }

        public void RemoveApplication(Application application)
        {
            //var app=Get(id);
            _context.Applications.Remove(application);
        }

        public bool UpdateApplication(Application item)
        {
            try
            {               
                _context.Entry(item).State = EntityState.Modified;                                
                return true;
            }
            catch (Exception e)
            {
                return false;
            }            
        }
        public void AddScreenshotToApplication(Application application, File image)
        {
            var screenshot = new Screenshot() { Application = application, Image = image };
            _context.Screenshots.Add(screenshot);         
        }
        public void DeleteFile(File file)
        {
            _context.Files.Remove(file);
        }
        public void DeleteScreenshot(Screenshot screenshot)
        {
            _context.Screenshots.Remove(screenshot);
            //_context.Files.Remove(screenshot.Image);
        }
        public void AddGrade(Grade grade)        
        {
            _context.Grades.Add(grade);
        }
        public void EditGrade(Grade grade)
        {
            _context.Entry(grade).State = EntityState.Modified;
        }
        public void DeleteGrade(Grade grade)
        {
            _context.Grades.Remove(grade);
        }




        public bool SaveChanges()
        {
            try
            {
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public void Dispose()
        {
            if (_context == null) return;
            _context.Dispose();
            _context = null;
        }


        public Tuple<Application, User> AddUserToApp(int appId, String userId)
        {
            var dbUser = _context.Users.Find(userId);
            if (dbUser == null)
                dbUser = new User() { UserId = userId };
            var dbApp = _context.Applications.Find(appId);
            if (dbApp == null) return null;

            dbApp.Users.Add(dbUser);
            dbUser.Applications.Add(dbApp);

            return new Tuple<Application, User>(dbApp, dbUser);

            
        }
        public  bool IsUserRegisteredToApp(string packageName,String userId)
        {
            bool? exists=(from u in _context.Users 
                       from a in u.Applications
                       where a.PackageName == packageName
                       && u.UserId == userId
                       select true).FirstOrDefault();

            return exists ?? false;                                    


        }


        public IEnumerable<Application> GetApplicationsByGradeName(string grade)
        {
            var applications = (from g in _context.Grades
                               from a in g.Applications
                               where g.Name.ToLower() == grade.ToLower()
                               select a).ToArray();

            return applications;
        }
    }
}
