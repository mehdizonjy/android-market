﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AndroidMarket.Helpers
{
    public enum FileType
    {
        Image, Apk
    }

    public static class FilesHelper
    {
        public static bool SaveFile(HttpPostedFileBase file,String name, FileType fileType)
        {
            try
            {
                var path = System.Web.HttpContext.Current.Server.MapPath(ResolvePath(fileType, name));
                file.SaveAs(path);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
      
        public static bool DeleteFile(String name,FileType fileType)
        {
            try
            {
                var path = System.Web.HttpContext.Current.Server.MapPath(ResolvePath(fileType, name));
                File.Delete(path);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
        public static string ResolvePath(FileType fileType, String fileName)
        {
            var folder=default(string);
            switch (fileType)
        	{
		        case FileType.Image:
                    folder="content/apks/images";
                    break;
                case FileType.Apk:
                    folder = "content/apks/apks/";
                    break;
            }

            //var result= System.Web.HttpContext.Current.Server.MapPath(System.IO.Path.Combine("~",folder,fileName));
            var result = Path.Combine("~", folder, fileName);
        return result;

        }        
    }
}