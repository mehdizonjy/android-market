﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AndroidMarket.Helpers
{
    public static class HtmlHelperExtension
    {
        public static MvcHtmlString FormatByte<T>(this HtmlHelper<T> helper, long size)
        {
            var str="";
            if (size >= 1024 * 1024)
                str = String.Format("{0}mb", ((float)size / (1024*1024)));
            else
                if (size >= 1024)
                    str = String.Format("{0}kb",  ((float)size/1024));
                else
                    str = String.Format("{0}byte", size);
            return new MvcHtmlString(str);
        }
        public static MvcHtmlString GetThumbnailPath<T>(this HtmlHelper<T> helper, String thumbnailPath)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var path = "~/content/apks/images/" + thumbnailPath;            

            return new MvcHtmlString(urlHelper.Content(path));
        }

        public static IHtmlString NL2Br<T>(this HtmlHelper<T> helper, String data)
        {

            var result = helper.Raw(helper.Encode(data).Replace(Environment.NewLine, "<br />"));
            return result;

        }
    }
}