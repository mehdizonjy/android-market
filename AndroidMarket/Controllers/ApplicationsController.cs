﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AndroidMarket.Models;
using System.IO;
using AndroidMarket.Helpers;
using AndroidMarket.Models.Entities;
using AndroidMarket.Models.ModelViews;

namespace AndroidMarket.Controllers
{
    public class ApplicationsController : Controller
    {
        //private AndroidMarketContext db = new AndroidMarketContext();
        private DbRepository _repository = new DbRepository();

        //
        // GET: /Default1/

        [Authorize]
        public ActionResult Index(String id)
        {
            if(String.IsNullOrEmpty(id))
                return View(_repository.Applications.ToList());
            else
            {
                return View(_repository.GetApplicationsByGradeName(id));
            }
        }

        //
        // GET: /Default1/Details/5

        public ActionResult Details(int id = 0)
        {
            var application = _repository.Applications.Include("Screenshots").Where(a => a.ApplicationId == id).FirstOrDefault();
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        //
        // GET: /Default1/Create
        [Authorize]
        public ActionResult Create()
        {
            var grades = _repository.Grades.ToList();//.GetGrades();
            return View(new CreateApplicationViewModel(null,grades));
        }

        //
        // POST: /Default1/Create

        [HttpPost]
        [Authorize]
        public ActionResult Create(Application application,
            HttpPostedFileBase Apk,            
            HttpPostedFileBase Thumbnail)
        
        {
            if(ModelState.IsValid)
            {
                var apkFile = new AndroidMarket.Models.Entities.File() { DownloadedCounter = 0, FileName = Apk.FileName, FileType = "Apk", Size = Apk.ContentLength, StorageName = Guid.NewGuid().ToString()+".apk" };
                var thumbnailFile = new AndroidMarket.Models.Entities.File() { DownloadedCounter = 0, FileName = Thumbnail.FileName, FileType = "Image", Size = Thumbnail.ContentLength, StorageName = Guid.NewGuid().ToString()+".jpg" };
                application.Apk = apkFile;
                application.Thumbnail = thumbnailFile;
                _repository.AddApplication(application);
                _repository.SaveChanges();                
                FilesHelper.SaveFile(Apk,apkFile.StorageName,FileType.Apk);
                FilesHelper.SaveFile(Thumbnail,thumbnailFile.StorageName, FileType.Image);              
                return RedirectToAction("Index");
            }
            var grades = _repository.Grades.ToList();//.GetGrades();
            return View(new CreateApplicationViewModel(application,grades));
        }
   
        //
        // GET: /Default1/Edit/5
        [Authorize]
        public ActionResult Edit(int id = 0)
        {
            
            Application application = _repository.Applications.Where(app=>app.ApplicationId==id).FirstOrDefault();//.Get(id);
            if (application == null)
            {
                return HttpNotFound();
            }
           
            return View(new CreateApplicationViewModel(application,_repository.Grades.ToArray()));//.GetGrades()));
        }

        //
        // POST: /Default1/Edit/5

        [HttpPost]
        [Authorize]
        public ActionResult Edit(Application application)
        {
            if (ModelState.IsValid)
            {
                _repository.UpdateApplication(application);
                _repository.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(new CreateApplicationViewModel(application,_repository.Grades.ToArray()));//.GetGrades()));
        }

        //
        // GET: /Default1/Delete/5
       [Authorize]
        public ActionResult Delete(int id = 0)
        {
            var application = _repository.Applications.Where(a=>a.ApplicationId==id).FirstOrDefault();//.Get(id);// db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        //
        // POST: /Default1/Delete/5

       [HttpPost, ActionName("Delete")]
       [Authorize]
       public ActionResult DeleteConfirmed(int id)
       {
           var app = (from a in _repository.Applications.Include("Apk").Include("Thumbnail").Include("Screenshots")
                      where a.ApplicationId == id
                      select a).FirstOrDefault();
           var screenshots = app.Screenshots.ToList();
           var thumbnail = app.Thumbnail;
           var apk = app.Apk;
           foreach (var screenshot in screenshots)
           {
               _repository.DeleteScreenshot(screenshot);
               FilesHelper.DeleteFile(screenshot.Image.StorageName, FileType.Image);
           }
           _repository.DeleteFile(thumbnail);
           _repository.DeleteFile(apk);
           _repository.RemoveApplication(app);
           _repository.SaveChanges();
           FilesHelper.DeleteFile(apk.StorageName, FileType.Apk);
           FilesHelper.DeleteFile(thumbnail.StorageName, FileType.Image);

           return RedirectToAction("Index");
       }

     

        protected override void Dispose(bool disposing)
        {
            _repository.Dispose();
            base.Dispose(disposing);
        }

        public FileResult Market()
        {
     
            var contentType = "application/octet-stream";
            var apkName="RainbowMarket.apk";
            var filePath =System.Web.HttpContext.Current.Server.MapPath(FilesHelper.ResolvePath(FileType.Apk, apkName));

            return File(filePath, contentType, apkName);
        }
    }
}