﻿using AndroidMarket.Helpers;
using AndroidMarket.Models;
using AndroidMarket.Models.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;   
using System.Web.Http;

namespace AndroidMarket.Api.Controllers
{
    public class ImagesController : ApiController
    {
        private DbRepository _repository = new DbRepository();

        public HttpResponseMessage Get(int id)
        {
            var app = _repository.Applications.Where(a => a.ApplicationId == id).FirstOrDefault();
            var path = FilesHelper.ResolvePath(FileType.Image, app.Thumbnail.StorageName);
            path = HttpContext.Current.Server.MapPath(path);
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            var fileStream = new FileStream(path, FileMode.Open);
            response.Content = new StreamContent(fileStream);
            response.Headers.Add("ContentType", "application/octet-stream");
            return response;
        }

    }
}
