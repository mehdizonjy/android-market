﻿using AndroidMarket.Helpers;
using AndroidMarket.Models;
using AndroidMarket.Models.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace AndroidMarket.Api.Controllers
{
    public class DownloadController : ApiController
    {
        private DbRepository _repository = new DbRepository();
          public HttpResponseMessage Get(int appId,string userId)
        {
            var tuple = _repository.AddUserToApp(appId, userId);

            HttpResponseMessage response = null;
            if (tuple==null)
            {
                response = new HttpResponseMessage(HttpStatusCode.Gone);
            }
            else
            {
                tuple.Item1.Apk.DownloadedCounter++;
                _repository.SaveChanges();
                var path = FilesHelper.ResolvePath(FileType.Apk, tuple.Item1.Apk.StorageName);
                path = HttpContext.Current.Server.MapPath(path);                
                response= new HttpResponseMessage(HttpStatusCode.OK);
                var fileStream = new FileStream(path, FileMode.Open);
                response.Content = new StreamContent(fileStream);
                response.Headers.Add("ContentType", "application/vnd.android.package-archive"); 

            }
            return response;
        }

      
    }
}
