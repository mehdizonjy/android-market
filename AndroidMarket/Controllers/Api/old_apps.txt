﻿using AndroidMarket.Models;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace AndroidMarket.Api.Controllers
{
    public class DownloadController : ApiController
    {
        private IApplicationRepository _repository = new DbApplicationsRepository();
  

        public void Get(int id)
        {
            var PhoneId = "ads";
            var app = _repository.Get(id);
            //if(app==null)

           // HttpResponseMessage response = null;
         //   if (app == null)
          //  {
           //     response = new HttpResponseMessage(HttpStatusCode.Gone);
            //}
            //else
           // {

                var apkPath = System.IO.Path.Combine("~/", "content/", app.ApkName );
                apkPath = HttpContext.Current.Server.MapPath(apkPath);

            //var keyFilePath=HttpContext.Current.Server.MapPath(System.IO.Path.Combine("~","content",System.IO.Path.GetRandomFileName()));
                var keyFilePath = HttpContext.Current.Server.MapPath(System.IO.Path.Combine("~", "content", "key.txt"));
            using (var stream = File.CreateText(keyFilePath))
            {
                stream.Write(PhoneId);
            }
            
            

             var randFileName= Path.GetRandomFileName();
             var tempFilePath = HttpContext.Current.Server.MapPath(System.IO.Path.Combine("~", "content", randFileName));
             using (var zipFile = new Ionic.Zip.ZipFile(apkPath))
             {
                 zipFile.RemoveSelectedEntries("key.txt", "assets");
                 zipFile.AddFile(keyFilePath, "assets");
                 zipFile.Save(tempFilePath);
                 
             }

            var response = HttpContext.Current.Response;
            response.Clear();
           
            response.WriteFile(tempFilePath);
            response.AddHeader("Content-Type", "text/html");
            response.AddHeader("ContentType", "application/octet-stream");
            response.Flush();

            System.IO.File.Delete(tempFilePath);
            File.Delete(keyFilePath);

            response.End();
        }
        private void ZipDirectory(String path, String directoryName, ZipFile zipFile)
        {
            foreach(var file in System.IO.Directory.GetFiles(path))
            {
                if(directoryName==null)
                    zipFile.AddFile(file);
                else
                    zipFile.AddFile(file, directoryName);
            }

            foreach (var directory in System.IO.Directory.GetDirectories(path))
                ZipDirectory(directory,System.IO.Path.GetFileName(directory), zipFile);

        }
    }
}
