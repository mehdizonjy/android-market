﻿using AndroidMarket.Models;
using AndroidMarket.Models.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AndroidMarket.Controllers.Api
{
    public class GradesController : ApiController
    {
        private DbRepository _context = new DbRepository();
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _context.Dispose();
        }
        public IEnumerable<Grade> Get()
        {
            var result = (from g in _context.Grades
                          select new Grade() { Name = g.Name, GradeId = g.GradeId, ApplicationsCount = g.Applications.Count }).ToArray();
            return result;
        }
    }

}
