﻿using AndroidMarket.Models;
using System.Data.Entity;
using System.Data;
using AndroidMarket.Models.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace AndroidMarket.Api.Controllers
{
    public class ApplicationsController : System.Web.Http.ApiController,IDisposable
    {
        private DbRepository _context = new DbRepository();
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _context.Dispose();
            _context = null;

        }

        // GET api/applications
        public IEnumerable<Models.Json.Application> Get()
        {
            var apps = (from app in _context.Applications.Include("Screenshots.Image").Include("Grade")
                        select app).ToArray();
            var result = new List<Application>();
            foreach (var app in apps)
                result.Add(new Application(app));
            return result;
        }

        // GET api/applications/5
        public Application Get(int id)
        {
            var application = (from app in _context.Applications.Include("Screenshots.Image").Include("Grade")
                               where app.ApplicationId==id
                               select app).FirstOrDefault();
            if (application == null) return null;
            var jsonApp = new Application(application);           
            return jsonApp;
        }

        [HttpGet]
        [ActionName("verify")]
        public HttpResponseMessage GetVerifyApp(String package, string userid)
        {

            bool? exists = (from u in _context.Users
                            from a in u.Applications
                            where a.PackageName == package
                            && u.UserId == userid
                            select true).FirstOrDefault();
            return this.Request.CreateResponse(HttpStatusCode.OK, new { Result = exists ?? false });
        }

        [HttpGet]
        [ActionName("ByGrade")]

        // GET api/applications/ByGrade/4
        public IEnumerable<Application> GetByGrade(int id)
        {
            if (id <= 0) return Get();
            var apps = (from app in _context.Applications.Include("Screenshots.Image").Include("Grade")
                        where app.Grade_GradeId==id
                        select app).ToArray();
            var result = new List<Application>();
            foreach (var app in apps)
                result.Add(new Application(app));
            return result;
        }


    }
}
