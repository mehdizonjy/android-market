﻿using AndroidMarket.Models;
using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AndroidMarket.Helpers;
using AndroidMarket.Models.Entities;

namespace AndroidMarket.Controllers
{
    public class ScreenshotsController : Controller,IDisposable
    {
        DbRepository _repository = new DbRepository();
        protected override void Dispose(bool disposing)
        {
            _repository.Dispose();
            _repository = null;
            base.Dispose(disposing);
      
        }

        //
        // GET: /Screenshots/

        public ActionResult Index(int id)
        {
             var app = _repository.Applications.Include("Screenshots").Where(a => a.ApplicationId == id).FirstOrDefault();
             if (app == null) return HttpNotFound();
            return View(app);
        }


        [HttpPost]
        public ActionResult Add(int ApplicationId, HttpPostedFileBase Image)
        {
            var app = _repository.Applications.Include("Screenshots").Where(a => a.ApplicationId == ApplicationId).FirstOrDefault();
            if (app == null)
                return HttpNotFound();
            if (Image != null)
            {
                var file = new AndroidMarket.Models.Entities.File() { DownloadedCounter = 0, FileName = Image.FileName, StorageName = Guid.NewGuid().ToString()+".jpg", Size = Image.ContentLength, FileType = "Image"};
                //var screenshot = new Screenshot() { Application = app, Image = file };
                _repository.AddScreenshotToApplication(app, file);
                _repository.SaveChanges();
                FilesHelper.SaveFile(Image, file.StorageName, FileType.Image);
            }
            return RedirectToAction("Index", new { id=ApplicationId});
        }

        
        public ActionResult Delete(int id)
        {
            var screenshot = _repository.Screenshots.Where(s => s.ScreenshotId == id).FirstOrDefault();
            if (screenshot != null)
            {
                var file = screenshot.Image;
                _repository.DeleteFile(file);
                _repository.DeleteScreenshot(screenshot);
                _repository.SaveChanges();
                FilesHelper.DeleteFile(file.StorageName, FileType.Image);               
                return RedirectToAction("Index",new{id=screenshot.Application_ApplicationId});
            }
            else
            return HttpNotFound();

        }

    }
}
