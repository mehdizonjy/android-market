﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AndroidMarket.Models;
using AndroidMarket.Models.Entities;

namespace AndroidMarket.Controllers
{
    public class GradesController : Controller
    {
        private DbRepository db = new DbRepository();

        //
        // GET: /Grades/

        [Authorize]
        public ActionResult Index()
        {            
            return View(db.Grades.ToList());
        }

        //
        // GET: /Grades/Details/5
        [Authorize]
        public ActionResult Details(int id = 0)
        {
            var grade = db.Grades.Where(g=>g.GradeId==id).FirstOrDefault();//.Find(id);
            if (grade == null)
            {
                return HttpNotFound();
            }
            return View(grade);
        }

        //
        // GET: /Grades/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Grades/Create

        [HttpPost]
        [Authorize]
        public ActionResult Create(Grade grade)
        {
            if (ModelState.IsValid)
            {
                db.AddGrade(grade);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(grade);
        }

        //
        // GET: /Grades/Edit/5
        [Authorize]
        public ActionResult Edit(int id = 0)
        {
            Grade grade = db.Grades.Where(g => g.GradeId == id).FirstOrDefault();
            if (grade == null)
            {
                return HttpNotFound();
            }
            return View(grade);
        }

        //
        // POST: /Grades/Edit/5

        [HttpPost]
        [Authorize]
        public ActionResult Edit(Grade grade)
        {
            if (ModelState.IsValid)
            {
                db.EditGrade(grade);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(grade);
        }

        //
        // GET: /Grades/Delete/5
        [Authorize]

        public ActionResult Delete(int id = 0)
        {
            Grade grade = db.Grades.Where(g=>g.GradeId==id).FirstOrDefault();//.Find(id);
            if (grade == null)
            {
                return HttpNotFound();
            }
            return View(grade);
        }

        //
        // POST: /Grades/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Grade grade = db.Grades.Where(g=>g.GradeId==id).FirstOrDefault();//.Find(id);
            db.DeleteGrade(grade);
            //db.Grades.Remove(grade);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}