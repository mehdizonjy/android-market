﻿using System;
using AndroidMarket.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AndroidMarket.Models.Entities;
using AndroidMarket.Models.ModelViews;

namespace AndroidMarket.Api.Controllers
{
    public class HomeController : Controller,IDisposable
    {
        private DbRepository _repository = new DbRepository();
        [OutputCache(Duration=30)]
        public ActionResult Index(string id)
        {
            var grades = _repository.Grades.ToArray();
            IEnumerable<Application> apps = null;
            if (!string.IsNullOrEmpty(id))
                apps = _repository.GetApplicationsByGradeName(id);
            return View(new GradesApplicationsViewModel(apps,grades));
        }
        new public  void Dispose()
        {
            _repository.Dispose();
            _repository = null;
            base.Dispose();
        }
        public ActionResult About()
        {
            return View();
        }

        

    }
}
