﻿using AndroidMarket.Models;
using AndroidMarket.Models.ModelViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AndroidMarket.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginUser user)
        {
            if (FormsAuthentication.Authenticate(user.UserName, user.Password))
            {
                FormsAuthentication.SetAuthCookie(user.UserName, false);
                return RedirectToAction("ControlPanel");
            }
            else
            {
                ViewData["LastLoginFailed"] = true;
                return View(user);
            }
        }

        [Authorize]
        public ActionResult ControlPanel()
        {
            return View();
        }
        [Authorize]
        public RedirectToRouteResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

    }
}