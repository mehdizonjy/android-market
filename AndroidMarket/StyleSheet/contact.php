<!DOCTYPE HTML>
<html dir="rtl">

<head>
  <title>قالب seascape</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.html">قالب CSS3<span class="logo_colour">_seascape_two</span></a></h1>
          <h2>قالب مواقع. بسيط. مُعاصر. </h2>
        </div>
      </div>
      <nav>
        <ul class="sf-menu" id="nav">
          <li><a href="index.html">البداية</a></li>
          <li><a href="examples.html">أمثله</a></li>
          <li><a href="page.html">صفحه</a></li>
          <li><a href="another_page.html">صفحة أخرى</a></li>
          <li><a href="#">قائمة Drop Down</a>
            <ul>
              <li><a href="#">عنصر أول</a></li>
              <li><a href="#">عنصر ثاني متفرع</a>
                <ul>
                  <li><a href="#">عنصر فرعي</a></li>
                  <li><a href="#">عنصر فرعي</a></li>
                  <li><a href="#">عنصر فرعي</a></li>
                  <li><a href="#">عنصر فرعي</a></li>
                  <li><a href="#">Sub Drop Down Five</a></li>
                </ul>
              </li>
              <li><a href="#">عنصر آخر</a></li>
              <li><a href="#">عنصر آخر</a></li>
              <li><a href="#">Drop Down ve</a></li>
            </ul>
          </li>
          <li class="selected"><a href="contact.php">اتصل بنا</a></li>
        </ul>
      </nav>
    </header>
    <div id="site_content">
      <ul id="images">
        <li><img src="images/1.jpg" width="600" height="300" alt="seascape_one" /></li>
        <li><img src="images/2.jpg" width="600" height="300" alt="seascape_two" /></li>
        <li><img src="images/3.jpg" width="600" height="300" alt="seascape_three" /></li>
        <li><img src="images/4.jpg" width="600" height="300" alt="seascape_four" /></li>
        <li><img src="images/5.jpg" width="600" height="300" alt="seascape_five" /></li>
        <li><img src="images/6.jpg" width="600" height="300" alt="seascape_seascape" /></li>
      </ul>
      <div id="sidebar_container">
        <div class="sidebar">
          <h3>آخر الأخبار</h3>
          <h4>تم إطلاق الموقع الجديد</h4>
          <h5>الأول من يونيو , 2012</h5>
          <p>2012 تم إطلاق التصميم الجديد لموقعنا هذا. ألقي نظره في الجوار على الموقع و أخبرنا  بما تظن.<br /><a href="#">اقرأ المزيد</a></p>
          <h4>خصم 20%</h4>
          <h5>الأول من مارس, 2012</h5>
          <p>لدينا عرض خاص بخصم  20%  يجميع العملاء الجديدين.<br /><a href="#">اقرأ المزيد</a></p>
        </div>
      </div>
      <div class="content">
        <h1>اتصل بنا</h1>
        <p>سلّم علينا , بإستخدام هذا النموذج :).</p>
        <?php
		  // مصمم القالب يقول بأنه غير مسؤوول عن هذا النموذج أو أي ضرر ناتج عنه
          // This PHP Contact Form is offered &quot;as is&quot; without warranty of any kind, either expressed or implied.
          // David Carter at www.css3templates.co.uk shall not be liable for any loss or damage arising from, or in any way
          // connected with, your use of, or inability to use, the website templates (even where David Carter has been advised
          // of the possibility of such loss or damage). This includes, without limitation, any damage for loss of profits,
          // loss of information, or any other monetary loss.

          // Set-up these 3 parameters
		  // 1. قم بكتابة بريدك الإلكتروني الذي ترغب بإستلام الرسائل منه
          // 1. Enter the email address you would like the enquiry sent to
		  // 2. اكتب عنوان الرسالة التي ستصلك على بريدك
          // 2. Enter the subject of the email you will receive, when someone contacts you
		  // 3. اكتب النص الذي ترغب بأن يظهر للمستخدم حين ارساله رسالة لك بنجاح
          // 3. Enter the text that you would like the user to see once they submit the contact form
          $to = 'enter email address here';
          $subject = 'Enquiry from the website';
          $contact_submitted = 'Your message has been sent.';

		  // لاتقم بتعديل أي شئ أسفل هذا السطر .. مالم تكن ذو معرفه بالبي اتش بي
          // Do not amend anything below here, unless you know PHP
          function email_is_valid($email) {
            return preg_match('/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i',$email);
          }
          if (!email_is_valid($to)) {
            echo '<p style="color: red;">يجب عليك كتابة بريدك صحيح للرسائل المُستقبلة من خلال تعديل المتغير to.</p>';
          }
          if (isset($_POST['contact_submitted'])) {
            $return = "\r";
            $youremail = trim(htmlspecialchars($_POST['your_email']));
            $yourname = stripslashes(strip_tags($_POST['your_name']));
            $yourmessage = stripslashes(strip_tags($_POST['your_message']));
            $contact_name = "Name: ".$yourname;
            $message_text = "Message: ".$yourmessage;
            $user_answer = trim(htmlspecialchars($_POST['user_answer']));
            $answer = trim(htmlspecialchars($_POST['answer']));
            $message = $contact_name . $return . $message_text;
            $headers = "From: ".$youremail;
            if (email_is_valid($youremail) && !eregi("\r",$youremail) && !eregi("\n",$youremail) && $yourname != "" && $yourmessage != "" && substr(md5($user_answer),5,10) === $answer) {
              mail($to,$subject,$message,$headers);
              $yourname = '';
              $youremail = '';
              $yourmessage = '';
              echo '<p style="color: blue;">'.$contact_submitted.'</p>';
            }
            else echo '<p style="color: red;">رجاءً أدخل اسمك , بريدك الإلكتروني , و جواب على سؤال الرياضيات البسيط الموجود في النموذج.</p>';
          }
          $number_1 = rand(1, 9);
          $number_2 = rand(1, 9);
          $answer = substr(md5($number_1+$number_2),5,10);
        ?>
        <form id="contact" action="contact.php" method="post">
          <div class="form_settings">
            <p><span>اسمك</span><input class="contact" type="text" name="your_name" value="<?php echo $yourname; ?>" /></p>
            <p><span>بريدك الإلكتروني</span><input class="contact" type="text" name="your_email" value="<?php echo $youremail; ?>" /></p>
            <p><span>الرسالة</span><textarea class="contact textarea" rows="5" cols="50" name="your_message"><?php echo $yourmessage; ?></textarea></p>
            <p style="line-height: 1.7em;">لمحاربة السبام, رجاءً أجب على السؤال التالي:</p>
            <p><span><?php echo $number_1; ?> + <?php echo $number_2; ?> = ?</span><input type="text" name="user_answer" /><input type="hidden" name="answer" value="<?php echo $answer; ?>" /></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="contact_submitted" value="إرسال" /></p>
          </div>
        </form>
      </div>
    </div>
    <footer>
      <p>حقوق النسخ &copy; اسم موقعك | <a href="http://www.css3templates.co.uk">تصميم css3templates.co.uk</a> و تعريب <a href="http://templaty.com">قالبي</a></p>
    </footer>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/jquery.kwicks-1.5.1.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#images').kwicks({
        max : 600,
        spacing : 2
      });
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
